#ifndef _POBR_MBASE_HPP_
#define _POBR_MBASE_HPP_

namespace pobr {
struct MBase {
  double m00, m10, m01, m11, m20, m02, m21, m12, m30, m03;
  void set_zero() {m00=m10=m01=m11=m20=m02=m21=m12=m30=m03=0.0;}
};
}

#endif // _POBR_MBASE_HPP_
