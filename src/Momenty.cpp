#include "Momenty.hpp"

#include "Segment.hpp"

namespace pobr {

Momenty::Momenty(const Segment& seg) {
  MBase b;
  calc([&](int p, int q) -> double {
    return m(p, q, seg);
  }, &b);
  full(&b);
}

Momenty::Momenty(const cv::Mat& im, uint32_t colour) {
  MBase b;
  calc([&](int p, int q) -> double {
    return m(p, q, im, colour);
  }, &b);
  full(&b);
}

Momenty::Momenty(const MBase& mb) {
  full(&mb);
}

void Momenty::calc(std::function<double(int, int)> m, MBase* p_b) {
  p_b->m00 = m(0, 0);
  p_b->m10 = m(1, 0);
  p_b->m01 = m(0, 1);
  p_b->m11 = m(1, 1);
  p_b->m20 = m(2, 0);
  p_b->m02 = m(0, 2);
  p_b->m21 = m(2, 1);
  p_b->m12 = m(1, 2);
  p_b->m30 = m(3, 0);
  p_b->m03 = m(0, 3);
}
void Momenty::full(const MBase* p_b) {
  double ii = p_b->m10 / p_b->m00;
  double jj = p_b->m01 / p_b->m00;
  double M11 = p_b->m11 - p_b->m10*p_b->m01 / p_b->m00;
  double M20 = p_b->m20 - p_b->m10*p_b->m10 / p_b->m00;
  double M02 = p_b->m02 - p_b->m01*p_b->m01 / p_b->m00;
  double M21 = p_b->m21 - 2 * p_b->m11 * ii - p_b->m20 * jj + 2 * p_b->m01 * ii * ii;
  double M12 = p_b->m12 - 2 * p_b->m11 * jj - p_b->m02 * ii + 2 * p_b->m10 * jj * jj;
  double M30 = p_b->m30 - 3* p_b->m20*ii + 2 * p_b->m10 * ii * ii;
  double M03 = p_b->m03 - 3* p_b->m02*jj + 2 * p_b->m01 * jj * jj;
  m_M[1] = (M20 + M02) / p_b->m00 / p_b->m00;
  m_M[2] = ((std::pow(M20 - M02, 2) + 4 * M11*M11) / std::pow(p_b->m00, 4));
  m_M[3] = (std::pow(M30 - 3*M12, 2) + std::pow(3*M21 + M03, 2)) / std::pow(p_b->m00, 5);
  m_M[4] = (std::pow(M30 + M12, 2) + std::pow(M21 + M03, 2)) / std::pow(p_b->m00, 5);
  m_M[5] = ((M30 - 3*M12)*(M30 + M12)*(std::pow(M30 + M12, 2) - 3 * std::pow(M21 + M03, 2))
    + (3 * M21 - M03)*(M21 + M03)*(3*std::pow(M30 + M12, 2) - std::pow(M21 + M03, 2)))
      / std::pow(p_b->m00, 10);
  m_M[6] = ((M20 - M02)*(std::pow(M30+M12, 2)-std::pow(M21+M03, 2)) + 4*M11*(M30+M12)*(M21+M03)) / std::pow(p_b->m00, 7);
  m_M[7] = (M20*M02 - M11*M11) / std::pow(p_b->m00, 4);
  m_M[8] = (M30*M12 + M21*M03 - M12*M12 - M21*M21) / std::pow(p_b->m00, 5);
  m_M[9] = (M20*(M21*M03 - M12*M12) + M02*(M03*M12 - M21*M21) - M11*(M30*M03 - M21*M12)) / std::pow(p_b->m00, 7);
  m_M[10] = (std::pow(M30*M03 - M12*M21, 2) - 4 * (M30*M12 - M21*M21)*(M03*M21 - M12)) / std::pow(p_b->m00, 10);
}

double Momenty::m(int p, int q, const Segment& seg) {
  double res = 0.0;
  for (const glm::ivec2& vec : seg.get_internal_bitmap()) {
    int j = vec.x, i = vec.y;
      res += std::pow((double)i, p) * std::pow((double)j, q) * seg.point(j, i);
    }
  return res;

}

}
