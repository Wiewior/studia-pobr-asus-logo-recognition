#ifndef _POBR_MODEL_LOAD_HPP_
#define _POBR_MODEL_LOAD_HPP_

#include <opencv2/imgcodecs.hpp>

#include "Segment.hpp"

namespace pobr {

class ModelLoad {
 public:
  static constexpr struct FromFile_t {} FromFile {};
  ModelLoad(FromFile_t, const char* path);
  const cv::Mat &get_image() const;
  const cv::Mat &get_preseg_image() const;
  const cv::Mat &get_second_image() const;
  const Segment &get_logo_segment() const;
  std::vector<Segment> get_segments() const;
 private:
  void prepare_to_segmentation();
  void segment();
 private:
  cv::Mat m_model_image_matrix;
  cv::Mat m_model_to_seg;
  cv::Mat m_second_img;
  Segment m_main_segment;
  std::vector<Segment> m_help_segments;
  std::vector<Segment> m_second_help_segments;
};

}

#endif // _POBR_MODEL_LOAD_HPP_
