#ifndef _POBR_SEGMENT_HPP_
#define _POBR_SEGMENT_HPP_

#include <vector>

#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/hash.hpp"
#include <glm/vec2.hpp>
#include <unordered_set>

#include "Momenty.hpp"

namespace pobr {

class Momenty;
class Segment {
  friend Momenty;
  glm::ivec2 m_offset;
  glm::ivec2 m_size;
  std::unordered_set<glm::ivec2> m_bitmap;
 public:
  Segment();
  bool point(int x, int y) const;
  bool point(glm::ivec2 p) const { return point(p.x, p.y); }
  size_t count() const;
  bool add(int x, int y);
  bool add(glm::ivec2 p) { return add(p.x, p.y); }
  Segment operator+(const Segment& o) const;
  void clear();
  const decltype(m_bitmap) &get_internal_bitmap() const { return m_bitmap; }

};

struct SegmentInfo {
  uint32_t area;
  uint32_t obwod;
  uint32_t colour;
  Momenty momenty;
  uint32_t border_area;
  glm::ivec2 topleft;
  glm::ivec2 botright;
};

}

#endif // _POBR_SEGMENT_HPP_
