#include "Segment.hpp"

namespace pobr {

Segment::Segment()
: m_offset(0,0)
, m_size(0,0)
, m_bitmap() {}

bool Segment::point(int x, int y) const {
  return m_bitmap.find(glm::ivec2(x, y)) != m_bitmap.end();
}

size_t Segment::count() const {
  return m_bitmap.size();;
}

bool Segment::add(int x, int y) {
  std::pair<decltype(m_bitmap)::iterator, bool> emplace_res = m_bitmap.emplace(x, y);
  if (!emplace_res.second) {
    return true;
  }
  if (m_bitmap.size() == 0) {
    m_offset = { x, y };
    m_size = { 1, 1 };
  } else {
    int
      t = m_offset.y - y,
      r = x - (m_offset.x + m_size.x),
      b = y - (m_offset.y + m_size.y),
      l = m_offset.x - x;
    if (t > 0) m_offset.y -= t, m_size.y += t;
    if (r > 0) m_size.x += r;
    if (b > 0) m_size.y += b;
    if (l > 0) m_offset.x -= l, m_size.x += l;
  }
  return false;
}

void Segment::clear() {
  m_offset = { 0, 0 };
  m_size = { 0, 0 };
  m_bitmap.clear();
}

Segment Segment::operator+(const Segment& o) const {
  Segment res = *this;
  glm::ivec2 topleft = glm::min(m_offset, o.m_offset);
  glm::ivec2 botright = glm::max(m_offset + m_size, o.m_offset + o.m_size);
  res.m_offset = topleft;
  res.m_size = botright - topleft;
  res.m_bitmap.merge( decltype(m_bitmap)  {o.m_bitmap});
  return res;
}

}
