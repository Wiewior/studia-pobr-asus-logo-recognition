#include "ModelLoad.hpp"

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <fmt/format.h>

#include "Procedures.hpp"

namespace pobr {

ModelLoad::ModelLoad(ModelLoad::FromFile_t, const char* path) {
  m_model_image_matrix = cv::imread(path);
  prepare_to_segmentation();
  segment();
}

const cv::Mat &ModelLoad::get_image() const {
  return m_model_image_matrix;
}

const cv::Mat &ModelLoad::get_preseg_image() const {
  return m_model_to_seg;
}

const cv::Mat &ModelLoad::get_second_image() const {
  return m_second_img;
}

const Segment &ModelLoad::get_logo_segment() const {
  return m_main_segment;
}

std::vector<Segment> ModelLoad::get_segments() const {
  assert(m_help_segments.size() == 7);
  return {
    m_main_segment,
    //m_help_segments[3],
    m_help_segments[5],
    m_help_segments[5] + m_help_segments[6],
    m_help_segments[6],
    //m_help_segments[5] + m_help_segments[6],
    m_help_segments[0] + m_help_segments[4],
    //m_help_segments[0] + m_help_segments[5],
    //m_help_segments[0] + m_help_segments[4] + m_help_segments[5]
    m_second_help_segments[4],
    m_second_help_segments[0] + m_second_help_segments[1] + m_second_help_segments[2],
  };
}

void ModelLoad::prepare_to_segmentation() {
  glm::ivec2 size(m_model_image_matrix.cols, m_model_image_matrix.rows);
  cv::Mat &gray = m_model_to_seg;
  gray = cv::Mat::zeros(size.y, size.x, CV_16UC1);
  for (int row = 0; row < size.y; ++row) {
    for (int col = 0; col < size.x; ++col) {
      const auto &px = m_model_image_matrix.at<cv::Vec3b>(row, col);
      uint16_t val = (px[0] + px[1] + px[2]) * 255/3;
      gray.at<uint16_t>(row, col) = val;
    }
  }
}

void ModelLoad::segment() {
  glm::ivec2 size(m_model_to_seg.cols, m_model_to_seg.rows);
  cv::Mat &gray = m_model_to_seg;
  m_main_segment.clear();
  for (int row = 0; row < size.y; ++row) {
    for (int col = 0; col < size.x; ++col) {
      uint16_t val = gray.at<uint16_t>(row, col);
      if (val < (1 << 15)) {
        m_main_segment.add(col, row);
        gray.at<uint16_t>(row, col) = 0xffff;
      } else {
        gray.at<uint16_t>(row, col) = 0;
      }
    }
  }

  cv::Mat gray2 = gray.clone();
  cv::Mat seg = cv::Mat::zeros(size.y, size.x, CV_8UC4);
  Segment s;

  uint32_t j = 1;
  glm::ivec2 v(0,0);

  while (Procedures::fetch_segment(gray2, seg, j, &v, nullptr)) {

    m_help_segments.push_back({});
    Segment &s = m_help_segments.back();

    for (int row = 0; row < size.y; ++row) {
      for (int col = 0; col < size.x; ++col) {
        uint32_t val = seg.at<uint32_t>(row, col);
        if (val == j) {
          s.add(col, row);
        }
      }
    }
    ++j;

  }

  gray2 = Procedures::dilate_rank(gray, 2, 12);
  gray2 = Procedures::dilate_rank(gray2, 2, 12);

  m_second_img = gray2.clone();


  j = 1;
  v = { 0,0 };
  seg = cv::Mat::zeros(size.y, size.x, CV_8UC4);
  while (Procedures::fetch_segment(gray2, seg, j, &v, nullptr)) {

    m_second_help_segments.push_back({});
    Segment &s = m_second_help_segments.back();

    for (int row = 0; row < size.y; ++row) {
      for (int col = 0; col < size.x; ++col) {
        uint32_t val = seg.at<uint32_t>(row, col);
        if (val == j) {
          s.add(col, row);
        }
      }
    }
    ++j;

  }

  fmt::print(stderr, "Pomocnicze segmenty modelu: {}\n", m_help_segments.size());

}

}
