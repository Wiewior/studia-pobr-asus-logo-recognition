#ifndef _POBR_MOMRNY_HPP_
#define _POBR_MOMRNY_HPP_

#include <functional>

#include "MBase.hpp"

#include <opencv2/imgcodecs.hpp>

#include <fmt/format.h>

namespace pobr {

struct Segment;
class Momenty {
 public:
  Momenty(const Segment& seg);
  Momenty(const cv::Mat&, uint32_t colour);
  Momenty(const MBase&);
  Momenty(){}
  ;
  static void calc(std::function<double(int, int)>, MBase*);
  void full(const MBase*);
  double m_M[11];
  static double m(int p, int q, const Segment& seg);
  static double m(int p, int q, const cv::Mat& im, uint32_t colour) {
    double res = 0.0;
    for (int i = 0; i < im.rows; ++i) {
      for (int j = 0; j < im.cols; ++j) {
        res += std::pow((double)i, p) * std::pow((double)j, q) * (im.at<uint32_t>(i, j) == colour);
      }
    }
    return res;
  }
};

}

#endif // _POBR_MOMRNY_HPP_
