#ifndef _POBR_PROCESSED_IMAGE_HPP_
#define _POBR_PROCESSED_IMAGE_HPP_

#include <algorithm>
#include <map>

#include <opencv2/imgcodecs.hpp>

#include "Segment.hpp"
#include "Procedures.hpp"
#include "Momenty.hpp"

namespace pobr {

class ProcessedImage {
  friend int;
 private:
  cv::Mat m_image_matrix;
  cv::Mat m_image_to_seg1;
  cv::Mat m_seg_preview1;
  cv::Mat m_seg_preview2;
  std::map<uint32_t, SegmentInfo> m_stored_segments;

 public:
  constexpr static struct FromFile_t {} FromFile {};
  ProcessedImage(FromFile_t, const char* path, const std::vector<Momenty>& cm) {
    m_image_matrix = cv::imread(path);
    prepare_to_segmentation();
    segment(cm);
  }
  const cv::Mat &get_image() const { return m_image_matrix; }
  const cv::Mat &get_preseg_image1() const { return m_image_to_seg1; }
  const cv::Mat &get_seg_prev1() const { return m_seg_preview1; }
  const cv::Mat &get2() const { return m_seg_preview2; }

  cv::Mat finalize() {
    glm::ivec2 size(m_image_matrix.cols, m_image_matrix.rows);
    cv::Mat res = m_image_matrix.clone();
    for (int row = 0; row < size.y; ++row) {
      for (int col = 0; col < size.x; ++col) {
        uint32_t c = m_seg_preview2.at<uint32_t>(row, col);
        auto it = m_stored_segments.find(c);
        if (it != m_stored_segments.end()) {
          it->second.topleft.x = std::min(it->second.topleft.x, col);
          it->second.topleft.y = std::min(it->second.topleft.y, row);
          it->second.botright.x = std::max(it->second.botright.x, col);
          it->second.botright.y = std::max(it->second.botright.y, row);
        }

      }
    }
    for (const auto &s : m_stored_segments) {
      for (int row = s.second.topleft.y; row < s.second.botright.y; ++row) {
        for (int col = s.second.topleft.x; col < s.second.botright.x; ++col) {
          int dist = 0xfffffff;
          dist = std::min(dist, col - s.second.topleft.x);
          dist = std::min(dist, -col + s.second.botright.x-1);
          dist = std::min(dist, row - s.second.topleft.y);
          dist = std::min(dist, -row + s.second.botright.y-1);
          if (dist < 5)
            res.at<cv::Vec3b>(row, col) = cv::Vec3b(0, 255, 0);
        }
      }
    }
    return res;
  }

 private:
  void prepare_to_segmentation() {
    glm::ivec2 size(m_image_matrix.cols, m_image_matrix.rows);
    cv::Mat gray = cv::Mat::zeros(size.y, size.x, CV_16UC1);

    for (int row = 0; row < size.y; ++row) {
      for (int col = 0; col < size.x; ++col) {
        const auto &px = m_image_matrix.at<cv::Vec3b>(row, col);
        uint16_t val = (px[0] + px[1] + px[2]) * 255/3;
        gray.at<uint16_t>(row, col) = val;
      }
    }

    gray = Procedures::rank(gray, 1, 4);
    gray = Procedures::adapt(gray, 125, 0.05);
    m_image_to_seg1 = gray.clone();
    return;
  }

  void segment(const std::vector<Momenty> cm) {
    const static double tol[11] = { -1.0, // <-- niepotrzebny, ale żeby było od 1 do 10
      0.9, 1.2, 8e14, 560000000000000.0, 1.8e29,
      -1390213123123123123903.0, 0.42, 5.6e13, 1.62e14, -1e17 };
    fmt::print(stderr, "Segmentacja\n");
    glm::ivec2 size(m_image_matrix.cols, m_image_matrix.rows);
    m_seg_preview1 = cv::Mat::zeros(size.y, size.x, CV_8UC4);
    glm::ivec2 v(0, 0);
    uint32_t j = 0xff008000;
    cv::Mat gray = m_image_to_seg1.clone();
    MBase mb;
    size_t segments = 0;
    uint32_t area = 0;
    uint32_t obwod = 0;
    static constexpr uint32_t jjump = 64;
    Procedures::change16(gray, 0x0000, 0xfffe);
    while (Procedures::fetch_segment(gray, m_seg_preview1, j, &v, &mb, &area, &obwod)) {
      Momenty momenty(mb);
      bool good = true;
      good = true;
      // WAŻNE: pole >= 200.0;
      // WAŻNE: obwód do pola
      double obwod_to_area = 1.0*obwod / (1.0*std::sqrt(area));
      if (area < 200.0 or obwod_to_area < 2.3 or obwod_to_area > 19.5) {
        good = false;
      }
      if (good) for (size_t k = 0; k < cm.size(); ++k) {
        for (int ii = 1; ii <= 10; ++ii) {
          double m = cm[k].m_M[ii];
          double ab = std::abs(momenty.m_M[ii] - m);
          if (tol[ii] >= 0.0 and ab > std::abs(m) * tol[ii]) {
            good = false;
            break;
          }
        }
        if (good) break;
      }
      if (good) {
        m_stored_segments.emplace(j, SegmentInfo{area, obwod, j, momenty, 0, { 0x0fffffff, 0x0fffffff}, { -1, -1 }});
      }
      j+=jjump;
      ++segments;
    }
    fmt::print(stderr, "Segmenty: {}\n", segments);
    filter_segments(cm);
  }

  void filter_segments(const std::vector<Momenty>& cm) {
    glm::ivec2 size(m_image_matrix.cols, m_image_matrix.rows);
    const auto px = [&](const cv::Mat& mat, int col, int row) -> uint32_t {
      return
        col >= 0 && col < size.x && row >= 0 && row < size.y ?
          mat.at<uint32_t>(row, col) :
          0;
    };
    for (long row = 0; row < size.y; ++row) {
      for (long col = 0; col < size.x; ++col) {
        uint32_t c = m_seg_preview1.at<uint32_t>(row, col);
        if (not m_stored_segments.count(c)) {
          m_seg_preview1.at<uint32_t>(row, col) = 0x0;
          continue;
        }
        long fr = 1;
        bool border = false;
        for (int y = row; y <= row + fr; ++y) {
          for (int x = y==row? col : col - fr; x <= col + fr; ++x) {
            if (px(m_seg_preview1, x, y) != c) {
              border = true;
              m_stored_segments.at(c).border_area += 1;
              break;
            }
            if (border) break;
          }
        }
      }
    }
    for (long row = 0; row < size.y; ++row) {
      for (long col = 0; col < size.x; ++col) {
        uint32_t c = m_seg_preview1.at<uint32_t>(row, col);
        if (m_stored_segments.count(c)) {
          auto &si = m_stored_segments.at(c);
          if (si.border_area >= 0.615 * std::pow(si.area, 0.915)
              or si.border_area >= 0.33 * si.area) {
            m_stored_segments.erase(c);
          }
        }
        if (not m_stored_segments.count(c)) {
          m_seg_preview1.at<uint32_t>(row, col) = 0;
          continue;
        }
      }
    }
    fmt::print(stderr, "Segmenty po odrzuceniu: {}\n", m_stored_segments.size());
    cv::Mat second_segmentation = cv::Mat::zeros(size.y, size.x, CV_16UC1);
    for (long row = 0; row < size.y; ++row) {
      for (long col = 0; col < size.x; ++col) {
        second_segmentation.at<uint16_t>(row, col) = m_seg_preview1.at<uint32_t>(row, col) != 0 ? 0xffff : 0;
      }
    }
    second_segmentation = Procedures::dilate_rank(second_segmentation, 1, 8);
    second_segmentation = Procedures::dilate_rank(second_segmentation, 1, 8);
    second_segmentation = Procedures::dilate_rank(second_segmentation, 3, 8);
    second_segmentation = Procedures::dilate_rank(second_segmentation, 3, 8);
    second_segmentation = Procedures::dilate_rank(second_segmentation, 2, 3);
    uint32_t j = 0xff008000;
    static constexpr uint32_t jjump = 64;
    m_seg_preview2 = cv::Mat::zeros(size.y, size.x, CV_8UC4);
    glm::ivec2 v{ 0, 0};
    m_stored_segments.clear();
    while (Procedures::fetch_segment(second_segmentation, m_seg_preview2, j, &v)) {
      j+=jjump;
      m_stored_segments.emplace(j, SegmentInfo{0, 0, j, Momenty(), 0, { 0x0fffffff, 0x0fffffff}, { -1, -1 }});
    }
  }
};

}


#endif // _POBR_PROCESSED_IMAGE_HPP_
