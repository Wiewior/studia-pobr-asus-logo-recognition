#include <iostream>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include "ModelLoad.hpp"
#include "ProcessedImage.hpp"
#include "Momenty.hpp"

int main(int argc, char **argv) {

  using namespace pobr;
  ModelLoad model(ModelLoad::FromFile, "../logo.png");

  const char *path[] = {
    "../IMG_20211220_224749.jpg",
    "../IMG_20211220_225457.jpg",
    "../IMG_20211220_225931.jpg",
    "../IMG_20211220_231754.jpg",
  };

  cv::namedWindow("Model", cv::WINDOW_NORMAL);
  cv::imshow("Model", model.get_image());
  cv::resizeWindow("Model", 500, 500);

  cv::namedWindow("Model czarno-biały", cv::WINDOW_NORMAL);
  cv::imshow("Model czarno-biały", model.get_preseg_image());
  cv::resizeWindow("Model czarno-biały", 500, 500);

  cv::namedWindow("Model2", cv::WINDOW_NORMAL);
  cv::imshow("Model2", model.get_second_image());
  cv::resizeWindow("Model2", 500, 500);

  std::vector<Momenty> momenty;
  for (auto& seg : model.get_segments()) {
    momenty.emplace_back(seg);
  }

  cv::waitKey(0);

  for (int i = 0; i < 4; ++i) {
    ProcessedImage image(ProcessedImage::FromFile, path[i], momenty);

    cv::namedWindow("Obraz", cv::WINDOW_NORMAL);
    cv::imshow("Obraz", image.get_image());
    cv::resizeWindow("Obraz", 800, 600);


    cv::namedWindow("Obraz czarno-bialy", cv::WINDOW_NORMAL);
    cv::imshow("Obraz czarno-bialy", image.get_preseg_image1());
    cv::resizeWindow("Obraz czarno-bialy", 800, 600);

    cv::namedWindow("Obraz segmenty", cv::WINDOW_NORMAL);
    cv::imshow("Obraz segmenty", image.get_seg_prev1());
    cv::resizeWindow("Obraz segmenty", 800, 600);

    auto result_image = image.finalize();
    cv::namedWindow("Wynik", cv::WINDOW_NORMAL);
    cv::imshow("Wynik", result_image);
    cv::resizeWindow("Wynik", 800, 600);

    cv::imwrite(fmt::format("result{}.png", i), result_image);

    cv::waitKey(0);
  }

  return 0;
}
