#ifndef _POBR_PROCEDURES_HPP_
#define _POBR_PROCEDURES_HPP_

#include <list>

#include <opencv2/imgcodecs.hpp>
#include <glm/vec2.hpp>

#include <fmt/format.h>

#include "MBase.hpp"

namespace pobr {

struct Procedures {


static cv::Mat fastblur(const cv::Mat& gray, size_t frame_round) {
  glm::ivec2 size(gray.cols, gray.rows);
  cv::Mat gray2 = cv::Mat::zeros(size.y, size.x, CV_16UC1);
  cv::Mat res = cv::Mat::zeros(size.y, size.x, CV_16UC1);
  size_t n = frame_round;
  std::vector<long> boxes(n, 3);
  double sigma = frame_round;
  double wI = std::sqrt((12.0*sigma*sigma/n)+1);
  long wl = wI;
  if (wl % 2 == 0) --wl;
  long wu = wl + 2;
  double mI = (12.0*sigma*sigma - n*wl*wl - 4*n*wl - 3*n)/(-4*wl-4);
  long m = std::round(mI);
  for (long i = 0; i < 3; ++i) {
    boxes[i] = i < m ? wl : wu;
  }

  cv::Mat one = gray.clone();
  assert(one.isContinuous());
  long w = size.x;
  long h = size.y;


  const auto boxblurh4 = [&](uint16_t *s, uint16_t *t, long fr) {
    double iarr = 1.0/(1.0+fr+fr);
    for (long row = 0; row < h; ++row) {
      long ti = row*w; long li=ti; long ri=ti+fr;
      double fv = s[ti]; double lv=s[ti+w-1];
      double val=(fr+1)*fv;
      for (long j = 0;   j <  fr;   ++j) { val += s[ti+j]; }
      for (long j = 0;   j <= fr;   ++j) { val += s[ri++] - fv  ;      t[ti++] = std::round(val*iarr); }
      for (long j = fr+1; j < w-fr; ++j) { val += s[ri++] - s[li++];   t[ti++] = std::round(val*iarr); }
      for (long j = w-fr; j < w;    ++j) { val += lv      - s[li++];   t[ti++] = std::round(val*iarr); }
    }
  };
  const auto boxblurt4 = [&](uint16_t *s, uint16_t *t, long fr) {
    double iarr = 1.0/(1.0+fr+fr);
    for (long col = 0; col < w; ++col) {
      long ti = col; long li=ti; long ri=ti+fr*w;
      double fv = s[ti]; double lv=s[ti+w*(h-1)];
      double val=(fr+1)*fv;
      for (long j = 0;   j <  fr;   ++j) { val += s[ti+j*w]; }
      for (long j = 0;   j <= fr;   ++j) { val += s[ri] - fv  ;    t[ti] = std::round(val*iarr);  ri+=w;ti+=w;}
      for (long j = fr+1; j < h-fr; ++j) { val += s[ri] - s[li];   t[ti] = std::round(val*iarr);  li+=w; ri+=w; ti+=w; }
      for (long j = h-fr; j < h;    ++j) { val += lv    - s[li];   t[ti] = std::round(val*iarr);  li+=w; ti+=w; }
    }
  };

  const auto boxblur4 = [&](uint16_t *s, uint16_t *t, long fr) -> void {
    for (long i = 0; i < w*h; ++i) t[i] = s[i];
    boxblurh4(t, s, fr);
    boxblurt4(s, t, fr);;
  };

  uint16_t *qwe = &one.at<uint16_t>(0, 0);
  uint16_t *ewq = &res.at<uint16_t>(0, 0);

  boxblur4(qwe, ewq, (boxes[0]-1)/2);
  boxblur4(ewq, qwe, (boxes[1]-1)/2);
  boxblur4(qwe, ewq, (boxes[2]-1)/2);

  return res;
}


static cv::Mat adapt(const cv::Mat& gray, size_t frame_round, double delta) {
  glm::ivec2 size(gray.cols, gray.rows);
  cv::Mat mean = fastblur(gray, frame_round);
  cv::Mat res = cv::Mat::zeros(size.y, size.x, CV_16UC1);
  for (int row = 0; row < size.y; ++row) {
    for (int col = 0; col < size.x; ++col) {
      double val = gray.at<uint16_t>(row, col);
      double mval = mean.at<uint16_t>(row, col);
      res.at<uint16_t>(row, col) = (val - mval + delta > 0.0) ? 0xffff : 0;
    }
  }
  return res;
}


static cv::Mat rank(const cv::Mat& gray, size_t frame_round, size_t level) {
  glm::ivec2 size(gray.cols, gray.rows);
  cv::Mat res = cv::Mat::zeros(size.y, size.x, CV_16UC1);

  int fr = frame_round;

  const auto px = [&](const cv::Mat& mat, int col, int row) -> uint16_t {
    return
      col >= 0 && col < size.x && row >= 0 && row < size.y ?
        mat.at<uint16_t>(row, col) :
        0;
  };

  std::vector<uint16_t> c(std::pow(fr*2+1, 2));

  for (int row = 0; row < size.y; ++row) {
    for (int col = 0; col < size.x; ++col) {
      int e = 0;
      for (int y = row - fr; y <= row + fr; ++y) {
        for (int x = col - fr; x <= col + fr; ++x) {
          c[e] = px(gray, x, y);
          ++e;
        }
      }
      std::sort(c.begin(), c.end());
      res.at<uint16_t>(row, col) = c[level];
    }
  }

  return res;
}


static cv::Mat dilate_rank(const cv::Mat& gray, size_t frame_round, size_t level) {
  glm::ivec2 size(gray.cols, gray.rows);
  cv::Mat res = cv::Mat::zeros(size.y, size.x, CV_16UC1);

  int fr = frame_round;

  const auto px = [&](const cv::Mat& mat, int col, int row) -> uint16_t {
    return
      col >= 0 && col < size.x && row >= 0 && row < size.y ?
        mat.at<uint16_t>(row, col) :
        0;
  };

  for (int row = 0; row < size.y; ++row) {
    for (int col = 0; col < size.x; ++col) {
      size_t whites = 0;
      for (int y = row - fr; y <= row + fr; ++y) {
        for (int x = col - fr; x <= col + fr; ++x) {
          if (px(gray, x, y) >= 0x8000) {
            ++whites;
            if (whites >= level) {
              res.at<uint16_t>(row, col) = 0xffff;
              break;
            }
          }
        }
        if (whites >= level) break;
      }
      if (whites < level)
        res.at<uint16_t>(row, col) = 0x0;
    }
  }

  return res;
}



static void change(cv::Mat& im, uint32_t from, uint32_t to) {
  glm::ivec2 size(im.cols, im.rows);

  for (int row = 0; row < size.y; ++row) {
    for (int col = 0; col < size.x; ++col) {
      if (im.at<uint32_t>(row, col) == from)
        im.at<uint32_t>(row, col) = to;
    }
  }
}

static void change16(cv::Mat& im, uint16_t from, uint16_t to) {
  glm::ivec2 size(im.cols, im.rows);

  for (int row = 0; row < size.y; ++row) {
    for (int col = 0; col < size.x; ++col) {
      if (im.at<uint16_t>(row, col) == from)
        im.at<uint16_t>(row, col) = to;
    }
  }
}


static bool fetch_segment(cv::Mat& gray, cv::Mat& out, uint32_t c, glm::ivec2 *p_vec, MBase* p_mbase = nullptr, uint32_t *p_area = nullptr, uint32_t *p_obwod = nullptr) {
  glm::ivec2 size(gray.cols, gray.rows);
  p_area && (*p_area = 0);
  p_obwod && (*p_obwod = 0);
  const auto px = [&](int col, int row) -> uint16_t {
    return
      col >= 0 && col < size.x && row >= 0 && row < size.y ?
        gray.at<uint16_t>(row, col) :
        0;
  };
  const auto shift_vec = [&]() -> bool {
    ++p_vec->x;
    if (p_vec->x >= size.x) {
      p_vec->x = 0;
      ++p_vec->y;
    }
    if (p_vec->y >= size.y)
      return false;
    return true;
  };
  while (px(p_vec->x, p_vec->y) == 0x0) {
    if (!shift_vec())
      return false;
  }
  std::vector<glm::ivec2> v_array(1, *p_vec);
  v_array.reserve(0x10000);
  bool good = false;
  uint16_t gcolour = 0;
  if (p_mbase) {
    p_mbase->set_zero();
  }
  while (v_array.size() > 0) {
    glm::ivec2 v = v_array.back();
    v_array.pop_back();
    uint16_t cc = px(v.x, v.y);
    if (cc != (uint16_t)0x0 and (!good or cc == gcolour)) {
      if (!good) gcolour = cc;
      good = true;
      out.at<uint32_t>(v.y, v.x) = c;
      gray.at<uint16_t>(v.y, v.x) = 0;
      p_area and ++*p_area;
      v_array.push_back(v + glm::ivec2(-1, 0));
      v_array.push_back(v + glm::ivec2(1, 0));
      v_array.push_back(v + glm::ivec2(0, -1));
      v_array.push_back(v + glm::ivec2(0, 1));
      if (p_mbase) {
        p_mbase->m00 += 1.0;
        p_mbase->m10 += v.y;
        p_mbase->m01 += v.x;
        p_mbase->m11 += v.y*v.x;
        p_mbase->m20 += v.y*v.y;
        p_mbase->m02 += v.x*v.x;
        p_mbase->m21 += v.y*v.y*v.x;
        p_mbase->m12 += v.x*v.x*v.y;
        p_mbase->m30 += v.y*v.y*v.y;
        p_mbase->m03 += v.x*v.x*v.x;
      }
    } else if (p_obwod and
               v.x >= 0 and
               v.y >= 0 and
               v.x < size.x and
               v.y < size.y and
               out.at<uint32_t>(v.y, v.x) != c) {
      ++*p_obwod;
    }
  }
  return good;
}

static void draw_segment(cv::Mat& out, const Segment& seg, uint32_t colour) {
  for (const auto& e : seg.get_internal_bitmap())
    if (e.x >= 0 && e.x < out.cols && e.y >= 0 && e.y < out.rows) {
      out.at<uint32_t>(e.y, e.x) = colour;
    }
}

};

}

#endif // _POBR_PROCEDURES_HPP_
